﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QKimai
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        //order dinner X
        //set defalut
        Timer autoSubmit_timer = new Timer();
        DateTime dtTmp;
        int week_count = 0;

        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            update_DateTime();
            BuildWeeklySubmitList();

            T_Project.Text = "2080";
            T_Task.Text = "1485";
            T_Hour.Text = "8";
            T_UserID.Text = "";
            btn_week_submit.Visibility = Visibility.Hidden;

            _Update_ProjName();
            _Update_TaskName();

            autoSubmit_timer.Elapsed+=autoSubmit_timer_Elapsed;
            autoSubmit_timer.Interval = 3600000;
            autoSubmit_timer.Stop();//MessageBox.Show(((int)DateTime.Now.ToLocalTime().DayOfWeek).ToString());

            autoSubmitStackPanel.Visibility = Visibility.Collapsed;

            com_proj.Items.Add(new KeyValuePair<string, string>("General", "548"));
            /*
            com_proj.Items.Add(new KeyValuePair<string, string>("Preload Utilities", "832"));
            com_proj.Items.Add(new KeyValuePair<string, string>("Preload Process", "833"));
            com_proj.Items.Add(new KeyValuePair<string, string>("Acer_Control_Center_V_2_0", "1475"));
            com_proj.Items.Add(new KeyValuePair<string, string>("Acer_Office_Manager_V_2_0", "1647"));
            com_proj.Items.Add(new KeyValuePair<string, string>("Amundsen_Windows_Client_V_1", "1724"));
            com_proj.Items.Add(new KeyValuePair<string, string>("Acer_Boutique_V_1_0", "1734"));
            com_proj.Items.Add(new KeyValuePair<string, string>("Smart Building", "1856"));
            com_proj.Items.Add(new KeyValuePair<string, string>("Artificial Intelligence Projects", "1891"));
            */
            com_proj.Items.Add(new KeyValuePair<string, string>("VeriSee_AI", "2079"));
            com_proj.Items.Add(new KeyValuePair<string, string>("CTC_AI", "2080"));

            com_task.Items.Add(new KeyValuePair<string, string>("Day-off", "447"));
            com_task.Items.Add(new KeyValuePair<string, string>("====================", "0"));

            com_task.Items.Add(new KeyValuePair<string, string>("Verisee-Research", "1479"));
            com_task.Items.Add(new KeyValuePair<string, string>("Verisee-Training", "1480"));
            com_task.Items.Add(new KeyValuePair<string, string>("Verisee-Coordination", "1481"));
            com_task.Items.Add(new KeyValuePair<string, string>("Verisee-Project Implementation", "1482"));
            com_task.Items.Add(new KeyValuePair<string, string>("Verisee-Test & Deployment", "1483"));
            com_task.Items.Add(new KeyValuePair<string, string>("====================", "0"));
            com_task.Items.Add(new KeyValuePair<string, string>("CTC-Research", "1484"));
            com_task.Items.Add(new KeyValuePair<string, string>("CTC-Training", "1485"));
            com_task.Items.Add(new KeyValuePair<string, string>("CTC-Coordination", "1486"));
            com_task.Items.Add(new KeyValuePair<string, string>("CTC-Project Implementation", "1487"));
            com_task.Items.Add(new KeyValuePair<string, string>("CTC-Test & Deployment", "1488"));
            /*
            com_task.Items.Add(new KeyValuePair<string, string>("Architecture Design", "630"));
            com_task.Items.Add(new KeyValuePair<string, string>("Component Implementation", "677"));
            com_task.Items.Add(new KeyValuePair<string, string>("Alpha", "678"));
            com_task.Items.Add(new KeyValuePair<string, string>("Beta", "679"));
            com_task.Items.Add(new KeyValuePair<string, string>("RC", "680"));
            com_task.Items.Add(new KeyValuePair<string, string>("RTM", "681"));
            com_task.Items.Add(new KeyValuePair<string, string>("Specification", "708"));
            com_task.Items.Add(new KeyValuePair<string, string>("System Architecture Study / Implemenation", "1008"));
            com_task.Items.Add(new KeyValuePair<string, string>("====================", "0"));
            com_task.Items.Add(new KeyValuePair<string, string>("Machine Learning study", "1069"));
            com_task.Items.Add(new KeyValuePair<string, string>("Deep Learning study", "1070"));
            com_task.Items.Add(new KeyValuePair<string, string>("Basic Platform implementation", "1071"));
            com_task.Items.Add(new KeyValuePair<string, string>("Sensor Deployment implementation", "1072"));
            com_task.Items.Add(new KeyValuePair<string, string>("Image Recognition Study", "1073"));
            com_task.Items.Add(new KeyValuePair<string, string>("Speech Interaction Study", "1074"));
            com_task.Items.Add(new KeyValuePair<string, string>("Project implementation", "1075"));
            com_task.Items.Add(new KeyValuePair<string, string>("Mobile Robot Grand Challenge", "1076"));
            com_task.Items.Add(new KeyValuePair<string, string>("Feasibility Study", "1077"));
            com_task.Items.Add(new KeyValuePair<string, string>("Visit Customers or Vendors", "1078"));
            */
        }

        private void BuildWeeklySubmitList()
        {
            int weekday = Math.Min(((int)dtTmp.DayOfWeek == 0) ? 7 : (int)dtTmp.DayOfWeek, 5);
            string[] weekStr={"","Mon","Tue","Wed","Thu","Fri"};

            weeklySubmitStackPanel.Children.Clear();
            for (int i = weekday+1 ; i <= weekday + 7; i++)
            {
                if(i==6 || i==7)
                {
                    continue;
                }

                CheckBox tmpCB = new CheckBox();
                tmpCB.Margin = new Thickness(0,0,12,0);
                tmpCB.Content = weekStr[i%7];
                tmpCB.Foreground = new SolidColorBrush(Color.FromRgb(255,255,255));
                tmpCB.VerticalAlignment = VerticalAlignment.Center;
                tmpCB.Click += WeekCheckBoxClick;

                weeklySubmitStackPanel.Children.Add(tmpCB);
            }

            int offset=-6;
            while(((int)dtTmp.AddDays(offset).DayOfWeek)==6 ||
                ((int)dtTmp.AddDays(offset).DayOfWeek)==0)
            {
                offset++;
            }
            date_rangeText.Text = dtTmp.AddDays(offset).ToString("yyyy.MM.dd") + " ~ "
                + T_Date.Text;
        }

        private void update_DateTime()
        {
            dtTmp = DateTime.Now.ToLocalTime();
            T_Date.Text = dtTmp.ToString("yyyy.MM.dd");
            T_Time.Text = dtTmp.ToString("HH:mm:ss");
        }

        private void refreshBtn_Click(object sender, RoutedEventArgs e)
        {
            update_DateTime();
            BuildWeeklySubmitList();
        }

        private void submitBtn_Click(object sender, RoutedEventArgs e)
        {
            if (T_Task.Text == "0") return;
            
            btn_submit.IsEnabled = false;

            update_DateTime();
            string url = "http://timecard.acer.com.tw/kimai/extensions/ki_timesheets/processor.php?pct_ID=" + T_Project.Text + "&evt_ID=" + T_Task.Text + "&edit_in_day=" + T_Date.Text + "&edit_out_day=" + T_Date.Text + "&edit_in_time=00:00:00&edit_out_time=" + T_Time.Text + "&edit_duration=" + T_Hour.Text + "&rate=&zlocation=&trackingnr=&comment=&comment_type=0&id=0&axAction=add_edit_record";
            //MessageBox.Show(url);
            System.Diagnostics.Process.Start(url);

            btn_submit.IsEnabled = true;
        }

        private void submitBtn_Click(DateTime DT)
        {
            update_DateTime();
            string url = "http://timecard.acer.com.tw/kimai/extensions/ki_timesheets/processor.php?pct_ID=" + T_Project.Text + "&evt_ID=" + T_Task.Text + "&edit_in_day=" + DT.ToString("yyyy.MM.dd") + "&edit_out_day=" + T_Date.Text + "&edit_in_time=00:00:00&edit_out_time=" + T_Time.Text + "&edit_duration=" + T_Hour.Text + "&rate=&zlocation=&trackingnr=&comment=&comment_type=0&id=0&axAction=add_edit_record";
            //MessageBox.Show(url);
            System.Diagnostics.Process.Start(url);
            System.Threading.Thread.Sleep(2600);
        }

        private void DBDBtn_Click(object sender, RoutedEventArgs e)
        {
            string url = "http://woodstock.acer.com.tw/dbd/?empid=" + T_UserID.Text + "&pwd=" + T_Pwd_.Password;
            //MessageBox.Show(url);
            System.Diagnostics.Process.Start(url);
            T_Pwd_.Password = "";
        }

        private void Update_ProjName(object sender, KeyEventArgs e)
        {
            //_Update_ProjName();
        }

        private void Update_TaskName(object sender, KeyEventArgs e)
        {
            //_Update_TaskName();
        }

        private void _Update_ProjName()
        {
            string str = "";
            switch (T_Project.Text)
            {
                case "548":
                    str = "General";
                    break;
                case "832":
                    str = "Preload Utilities";
                    break;
                case "833":
                    str = "Preload Process";
                    break;
                case "1475":
                    str = "Acer_Control_Center_V_2_0";
                    break;
                case "1647":
                    str = "Acer_Office_Manager_V_2_0";
                    break;
                case "1724":
                    str = "Amundsen_Windows_Client_V_1";
                    break;
                case "1734":
                    str = "Acer_Boutique_V_1_0";
                    break;
                case "1856":
                    str = "Smart Building";
                    break;
                case "1891":
                    str = "Artificial Intelligence Projects";
                    break;
                case "2079":
                    str = "VeriSee_AI";
                    break;
                case "2080":
                    str = "CTC_AI";
                    break;

                default:
                    str = "";
                    break;
            }
            if (str == "") com_proj.Text = str;

            Full_Project.Text = str;
        }

        private void _Update_TaskName()
        {
            string str = "";
            switch (T_Task.Text)
            {
                case "412":
                    str = "Initialization";
                    break;
                //case "413":
                //    str = "Maintenance";
                //    break;
                case "447":
                    str = "Day-off";
                    break;
                case "630":
                    str = "Architecture Design";
                    break;
                case "677":
                    str = "Component Implementation";
                    break;
                case "678":
                    str = "Alpha";
                    break;
                case "679":
                    str = "Beta";
                    break;
                case "680":
                    str = "RC";
                    break;
                case "681":
                    str = "RTM";
                    break;
                case "708":
                    str = "Specification";
                    break;

                case "1479":
                    str = "Research";
                    break;
                case "1480":
                    str = "Training";
                    break;
                case "1481":
                    str = "Coordination";
                    break;
                case "1482":
                    str = "Project Implementation";
                    break;
                case "1483":
                    str = "Test & Deployment";
                    break;

                case "1484":
                    str = "Research";
                    break;
                case "1485":
                    str = "Training";
                    break;
                case "1486":
                    str = "Coordination";
                    break;
                case "1487":
                    str = "Project Implementation";
                    break;
                case "1488":
                    str = "Test & Deployment";
                    break;
                default:
                    str = "";
                    break;
            }
            if (str == "") com_task.Text = str;

            Full_Task.Text = str;
        }

        private void autoSubmit_click(object sender, RoutedEventArgs e)
        {
            switch(_autoSubmit.IsChecked)
            {
                case true:
                    btn_submit.IsEnabled = false;
                    btn_week_submit.IsEnabled = false;
                    T_Project.IsEnabled = false;
                    T_Task.IsEnabled = false;
                    T_Hour.IsEnabled = false;
                    autoSubmit_timer.Start();
                    break;

                case false:
                    btn_submit.IsEnabled = true;
                    btn_week_submit.IsEnabled = true;
                    T_Project.IsEnabled = true;
                    T_Task.IsEnabled = true;
                    T_Hour.IsEnabled = true;
                    autoSubmit_timer.Stop();
                    break;
            }
        }

        private void weekSubmitBtnClick(object sender, RoutedEventArgs e)
        {
            if (T_Task.Text == "0") return;

            btn_week_submit.IsEnabled = false;
            weeklySubmitStackPanel.IsEnabled = false;

            MessageBox.Show("Now Submit the Chosen Day(s)");

            int weektoday = Math.Min(((int)dtTmp.DayOfWeek == 0) ? 7 : (int)dtTmp.DayOfWeek, 5);
            int offset = -6;

            for (int i = 0; i < weeklySubmitStackPanel.Children.Count; i++)
            {
                while (((int)dtTmp.AddDays(offset).DayOfWeek) >= 6 ||
                        ((int)dtTmp.AddDays(offset).DayOfWeek) <= 0)
                {
                    offset++;
                }

                if(( (CheckBox)weeklySubmitStackPanel.Children[i]).IsChecked==true)
                {
                    submitBtn_Click(dtTmp.AddDays(offset));
                }
                offset++;
            }
            btn_week_submit.IsEnabled = true;
            weeklySubmitStackPanel.IsEnabled = true;
        }

        private void secretClick(object sender, MouseButtonEventArgs e)
        {
            switch(autoSubmitStackPanel.Visibility)
            {
                case Visibility.Collapsed:
                    autoSubmitStackPanel.Visibility = Visibility.Visible;
                    break;
                case Visibility.Visible:
                    autoSubmitStackPanel.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        private void autoSubmit_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            update_DateTime();
            if (dtTmp.Hour.ToString() == set_Hour.Text &&
                (int)dtTmp.DayOfWeek >= 1 && (int)dtTmp.DayOfWeek <= 5)
            {
                submitBtn_Click(sender, new RoutedEventArgs());
            }
        }

        private void WeekCheckBoxClick(object sender, RoutedEventArgs e)
        {
            switch (((CheckBox)sender).IsChecked)
            {
                case true:
                    week_count++;
                    break;

                case false:
                    week_count--;
                    break;
            }
            if (week_count > 0)
            {
                btn_submit.Visibility = Visibility.Hidden;
                btn_week_submit.Visibility = Visibility.Visible;
            }
            else
            {
                btn_submit.Visibility = Visibility.Visible;
                btn_week_submit.Visibility = Visibility.Hidden;
            }
        }

        //private void com_proj_change(object sender, SelectionChangedEventArgs e)
        //{
        //    string text = ((KeyValuePair<string, string>)e.AddedItems[0]).Value;
        //    T_Project.Text = text;
        //}

        private void com_proj_close(object sender, EventArgs e)
        {
            try
            {
                string text = ((KeyValuePair<string, string>)(sender as ComboBox).SelectionBoxItem).Value;
                T_Project.Text = text;
            }
            catch
            {
                com_proj.Text = "";
            }
        }
        private void com_task_close(object sender, EventArgs e)
        {
            try
            {
                string text = ((KeyValuePair<string, string>)(sender as ComboBox).SelectionBoxItem).Value;
                T_Task.Text = text;
            }
            catch
            {
                com_task.Text = "";
            }
        }
    }
}
